public class Refrigerator{
	
	private int coldestTemperature;
	private int numOfCompartments;
	private boolean smartFridgeOrNot;
	
	public int getColdestTemp(){
		return this.coldestTemperature;
	}
	public int getNumCompart(){
		return this.numOfCompartments;
	}
	public boolean getSmartFridge(){
		return this.smartFridgeOrNot;
	}
	
	public void fridge1(int coldestTemperature, int numOfCompartments, boolean smartFridgeOrNot){
		this.coldestTemperature = coldestTemperature;
		this.numOfCompartments = numOfCompartments;
		this.smartFridgeOrNot = smartFridgeOrNot;
	}
	
	public void setColdestTemp(int coldestTemperature){
		this.coldestTemperature = coldestTemperature;
	}
	
	public void setNumCompart(int numOfCompartments){
		this.numOfCompartments = numOfCompartments;
	}
	public void setSmartFridge(boolean smartFridgeOrNot){
		this.smartFridgeOrNot = smartFridgeOrNot;
	}
	
	
	public boolean doesItFreeze(){
		if(this.coldestTemperature <= 0){
			return true;
		}
		else{
			return false;
		}
	}
	
	public boolean connectWifi(){
		if(this.smartFridgeOrNot == true){
			return true;
		}
		else{
			return false;
		}
	}
	
	public boolean broken(int coldestTemperature){
		this.helperValidation(coldestTemperature);
		if(this.coldestTemperature > 10){
			return true;
		}
		else{
			return false;
		}
	}
	
	private void helperValidation(int temp){
		if (temp > 30){
			System.out.println("Your refrigerator is very broken");
		}
	}
}
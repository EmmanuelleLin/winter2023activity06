import java.util.Scanner;
class ApplianceStore{
	public static void main (String[] args){
		
		Scanner keyboard = new Scanner(System.in);
		Refrigerator[] aBunchOfRefrigerators = new Refrigerator[2];
		
			for(int i = 0; i < aBunchOfRefrigerators.length; i++){
				aBunchOfRefrigerators[i] = new Refrigerator();
				
				int gettingColdestTemp = keyboard.nextInt();
				int gettingNumOfCompart = keyboard.nextInt();
				boolean gettingSmartFridge = keyboard.nextBoolean();
				
				aBunchOfRefrigerators[i].fridge1(gettingColdestTemp, gettingNumOfCompart, gettingSmartFridge); 
			}
			
			int newGettingColdestTemp = keyboard.nextInt();
			int newGettingNumOfCompart = keyboard.nextInt();
			boolean newGettingSmartFridge = keyboard.nextBoolean();
			
			System.out.println(aBunchOfRefrigerators[1].getColdestTemp());
			aBunchOfRefrigerators[1].fridge1(newGettingColdestTemp, newGettingNumOfCompart, newGettingSmartFridge);
			System.out.println(aBunchOfRefrigerators[1].getColdestTemp());
			
			System.out.println(aBunchOfRefrigerators[1].broken(aBunchOfRefrigerators[1].getColdestTemp()));
	}
}